#pragma once

#include <stdio.h>

FILE *open_file(char const *path, char const *mode);
void close_file(FILE *file);
