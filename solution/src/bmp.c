#include "bmp.h"

#define HEAD_BMP_SIGNATURE 0x4D42
#define HEAD_BMP_RESERVED 0
#define HEAD_BMP_SIZE 40
#define HEAD_BMP_PLANES 1
#define HEAD_BMP_BITCOUNT 24
#define HEAD_BMP_COMPRESSION 0
#define HEAD_BMP_X_PELS_PER_METER 0
#define HEAD_BMP_Y_PELS_PER_METER 0
#define HEAD_BMP_CLR_USED 0
#define HEAD_BMP_CLR_IMPORTANT 0

struct __attribute__((packed)) bmp_header {
    uint16_t bfType;
    uint32_t bfileSize;
    uint32_t bfReserved;
    uint32_t bOffBits;
    uint32_t biSize;
    uint32_t biWidth;
    uint32_t biHeight;
    uint16_t biPlanes;
    uint16_t biBitCount;
    uint32_t biCompression;
    uint32_t biSizeImage;
    uint32_t biXPelsPerMeter;
    uint32_t biYPelsPerMeter;
    uint32_t biClrUsed;
    uint32_t biClrImportant;
};

static uint32_t count_padding(uint32_t const width) {
    return (4 - (width * sizeof(struct pixel)) % 4) % 4;
}

enum read_status from_bmp(FILE *in, struct image *img) {
    struct bmp_header head = {0};

    if (in == NULL) {
        return READ_INVALID_FILE;
    }
    if (fread(&head, sizeof(struct bmp_header), 1, in) != 1) {
        return READ_INVALID_FILE;
    }

    if (head.bfType != HEAD_BMP_SIGNATURE) {
        return READ_INVALID_SIGNATURE;
    }
    if (head.biBitCount != HEAD_BMP_BITCOUNT) {
        return READ_INVALID_BITS;
    }
    if (head.biSize != HEAD_BMP_SIZE) {
        return READ_INVALID_HEADER;
    }
    if (head.biCompression != HEAD_BMP_COMPRESSION) {
        return READ_INVALID_COMPRESSION;
    }

    const uint32_t PADDING = count_padding(head.biWidth);
    *img = image_create(head.biWidth, head.biHeight);
    if (img->data == NULL) {
        return READ_INVALID_FILE;
    }
    for (size_t r = 0; r < img->height; r++) {
        if (fread(img->data + r * img->width, sizeof(struct pixel), img->width, in) != img->width) {
            return READ_INVALID_FILE;
        }
        fseek(in, PADDING, SEEK_CUR);
    }
    return READ_OK;
}

static uint32_t count_img_size(struct image const *img) {
    return (img->width * sizeof(struct pixel) + count_padding(img->width)) * img->height;
}

enum write_status to_bmp(FILE *out, struct image const *img) {
    const uint32_t PADDING = count_padding(img->width);

    struct bmp_header head = {
        .bfType = HEAD_BMP_SIGNATURE,
        .bfileSize = sizeof(head) + count_img_size(img),
        .bfReserved = HEAD_BMP_RESERVED,
        .bOffBits = sizeof(head),
        .biSize = HEAD_BMP_SIZE,
        .biWidth = img->width,
        .biHeight = img->height,
        .biPlanes = HEAD_BMP_PLANES,
        .biBitCount = HEAD_BMP_BITCOUNT,
        .biCompression = HEAD_BMP_COMPRESSION,
        .biSizeImage = count_img_size(img),
        .biXPelsPerMeter = HEAD_BMP_X_PELS_PER_METER,
        .biYPelsPerMeter = HEAD_BMP_Y_PELS_PER_METER,
        .biClrUsed = HEAD_BMP_CLR_USED,
        .biClrImportant = HEAD_BMP_CLR_IMPORTANT};

    fwrite(&head, sizeof(head), 1, out);

    for (size_t r = 0; r < img->height; r++) {
        if (fwrite(img->data + r * img->width, sizeof(struct pixel), img->width, out) != img->width) {
            return WRITE_ERROR;
        }
        if (fseek(out, PADDING, SEEK_CUR) != 0) {
            return WRITE_ERROR;
        }
    }

    return WRITE_OK;
}
