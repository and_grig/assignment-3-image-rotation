#include "img.h"

#include <stdint.h>
#include <stdlib.h>

struct image image_create(uint64_t const width, uint64_t const height) {
    struct image img = {
        .width = width,
        .height = height,
        .data = malloc(width * height * sizeof(struct pixel))};
    return img;
}

void image_free(struct image *img) {
    if (img->data != NULL) {
        free(img->data);
        img->data = NULL;
    }
}
