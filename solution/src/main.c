#include <stdint.h>
#include <stdlib.h>

#include "bmp.h"
#include "file.h"
#include "img.h"
#include "transform.h"

int main(int argc, char **argv) {
    if (argc != 3) {
        fprintf(stderr, "Usage: %s <input file> <output file>\n", argv[0]);
        return 1;
    }
    FILE *in = open_file(argv[1], "rb");
    FILE *out = open_file(argv[2], "wb");

    struct image img = {0};
    enum read_status const rs = from_bmp(in, &img);

    if (rs != READ_OK) {
        image_free(&img);
        close_file(in);
        close_file(out);

        switch (rs) {
            case READ_OK:
                break;
            case READ_INVALID_SIGNATURE:
                fprintf(stderr, "Invalid signature\n");
                break;
            case READ_INVALID_BITS:
                fprintf(stderr, "BMP must be 24bit\n");
                break;
            case READ_INVALID_HEADER:
                fprintf(stderr, "Invalid header\n");
                break;
            case READ_INVALID_COMPRESSION:
                fprintf(stderr, "Compression is not allowed\n");
                break;
            case READ_INVALID_FILE:
                fprintf(stderr, "Invalid file\n");
                break;
        }
        return 1;
    }

    if (img.data == NULL) {
        fprintf(stderr, "Error: image data is NULL\n");
        close_file(in);
        close_file(out);
        return 1;
    }
    struct image rotated = rotate(img);

    image_free(&img);

    if (rotated.data == NULL) {
        fprintf(stderr, "Error: rotated data is NULL\n");
        close_file(in);
        close_file(out);
        return 1;
    }

    enum write_status const ws = to_bmp(out, &rotated);
    image_free(&rotated);

    if (ws != WRITE_OK) {
        close_file(in);
        close_file(out);

        switch (ws) {
            case WRITE_OK:
                break;
            case WRITE_ERROR:
                fprintf(stderr, "Write error\n");
                break;
        }

        return 1;
    }

    printf("Image saved in %s\n", argv[2]);

    close_file(in);
    close_file(out);

    return 0;
}
