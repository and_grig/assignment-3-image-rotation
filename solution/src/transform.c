#include "transform.h"

#include <stdlib.h>

struct image rotate(struct image const source) {
    struct image res = image_create(source.height, source.width);
    if (res.data == NULL) {
        return res;
    }
    for (size_t r = 0; r < res.height; r++) {
        for (size_t c = 0; c < res.width; c++) {
            res.data[r * res.width + c] = source.data[(source.height - c) * source.width + r - source.width];
        }
    }
    return res;
}
