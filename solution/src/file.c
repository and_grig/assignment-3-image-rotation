#include "file.h"

FILE *open_file(char const *path, char const *mode) {
    FILE *file = fopen(path, mode);
    if (file == NULL) {
        printf("Can't open file %s\n", path);
        return NULL;
    }
    return file;
}

void close_file(FILE *file) {
    if (file != NULL) {
        fclose(file);
    }
}
